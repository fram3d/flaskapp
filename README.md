# Task Manager

Web app

# Development Setup

Install python and pip on local machine

```bash
pip install virtualenv
python -m venv venv #/path/to/new/virtual/environment
source venv/bin/activate #activate virtual env
pip install -r requirements.txt
python3 ./init_db.py #initialize database

python3 ./run.py #run project
```

# Build app

```bash
cd build-deb/
make
```

# Build app

```bash
sudo apt install ./flaskapp.deb
```
