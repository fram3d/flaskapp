from flaskapp import db

class Table(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    variable1 = db.Column(db.Integer, nullable=False)
    variable2 = db.Column(db.Integer, nullable=False)

