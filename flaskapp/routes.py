from flask import render_template, request, redirect
from flaskapp import app, db
from flaskapp.functions import *
from flaskapp.models import *
import configparser

CONFIG_PATH = "/var/flaskapp/flaskapp/config.ini"

config = configparser.ConfigParser()
config.read(CONFIG_PATH)

CONFIGVARIABLE = config.get('sectionname', 'CONFIGVARIABLE')

@app.route('/', methods=['GET'])
def index():
    if request.method == 'GET':
        localvariable1 = "Placeholder1"
        localvariable1 = "Placeholder2"
        try:
            return render_template('index.html', pagevariable1 = localvariable1, pagevariable2 = localvariable2 )
        except:
            return 'Error retriving page'
    else:
        return 'HTTP request method not recogniezed'

@app.route('/submit', methods=['POST', 'GET'])
def submit():
    if request.method == 'GET':
        return render_template('submit.html')
    elif request.method == 'POST':
        userinput1 = request.form['forminput1']
        userinput2 = request.form['forminput2']
        sqlrow = Table(variable1 = int(userinput1), variable2 = int(userinput2))
        try:
            db.session.add(sqlrow)
            db.session.commit()
            return 'Row added'
        except:
            return 'Adding row to table failed'

    else:
        return 'HTTP request method not recogniezed'
